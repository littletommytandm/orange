package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/client"
	"net/http"
	userPb "orange/user-web/proto/user"
)

var (
	userClient userPb.UserService
)

func New() {
	userClient = userPb.NewUserService("orange.srv.user", client.DefaultClient)
}

// Error 错误结构体
type Error struct {
	Message string `json:"message"`
}

func SignIn(c *gin.Context) {
	type SignIn struct {
		UserName string `form:"username" binding:"required"`
		Pwd      string `form:"pwd" binding:"required"`
	}
	var payload SignIn
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": &Error{Message: "payload validation failed"}})
		return
	}
	rsp, _ := userClient.QueryUserByName(context.TODO(), &userPb.Request{UserName: payload.UserName})
	if rsp.Success != true {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": &Error{Message: "internal server error"}})
		return
	}
	if payload.Pwd != rsp.User.Pwd {
		c.JSON(http.StatusNotFound, gin.H{"success": false, "error": &Error{Message: "username or pwd incorrect"}})
		return
	}
	rsp.User.Pwd = ""
	c.JSON(http.StatusOK, gin.H{"success": rsp.Success, "user": rsp.User})
}

func SignUp(c *gin.Context) {
	type SignUp struct {
		UserName string `json:"username" binding:"required"`
		Email    string `json:"email" binding:"required"`
		Pwd      string `json:"pwd" binding:"required"`
	}

	var payload SignUp
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": &Error{Message: "payload validation failed"}})
		return
	}

	rsp, _ := userClient.CreateUser(context.TODO(), &userPb.CreateUserRequest{
		UserName: payload.UserName,
		Email:    payload.Email,
		Pwd:      payload.Pwd,
	})

	if rsp.Success != true {
		if rsp.Error.Code == 10001 {
			c.JSON(404, gin.H{"success": false, "error": &Error{Message: "该用户名或邮箱有被使用"}})
			return
		}
		c.JSON(500, gin.H{"success": false, "error": &Error{Message: "internal server error"}})
		return
	}
	c.JSON(http.StatusOK, gin.H{"success": rsp.Success, "user": rsp.User})
}
