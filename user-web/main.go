package main

import (
	"github.com/gin-gonic/gin"
	"github.com/micro/cli"
	"github.com/micro/go-micro/util/log"
	"orange/user-web/basic"

	"github.com/micro/go-micro/web"
	//"github.com/gin-gonic/gin"
	"orange/user-web/handler"
)

func main() {
	// New Basic
	basic.New()

	// create new web m
	m := web.NewService(
		web.Name("orange.web.user"),
		web.Version("latest"),
		web.Address(":8080"),
	)

	if err := m.Init(
		web.Action(
			func(c *cli.Context) {
				// 初始化handler
				handler.New()
			}),
	); err != nil {
		log.Fatal(err)
	}

	router := gin.Default()
	router.POST("/user/join", handler.SignUp)
	router.POST("/user/login", handler.SignIn)

	m.Handle("/", router)

	// run m
	if err := m.Run(); err != nil {
		log.Fatal(err)
	}
}
