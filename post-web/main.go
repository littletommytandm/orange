package main

import (
	"github.com/gin-gonic/gin"
	"github.com/micro/cli"
	"github.com/micro/go-micro/util/log"
	"github.com/micro/go-micro/web"
	"orange/post-web/basic"
	"orange/post-web/handler"
)

func main() {
	// New Basic
	basic.New()

	// create new web m
	m := web.NewService(
		web.Name("orange.web.post"),
		web.Version("latest"),
		web.Address(":8080"),
	)

	if err := m.Init(
		web.Action(
			func(c *cli.Context) {
				// 初始化handler
				handler.New()
			}),
	); err != nil {
		log.Fatal(err)
	}

	router := gin.Default()
	router.POST("/posts/create", handler.CreatePost)
	router.GET("/posts", handler.ListPosts)
	router.GET("/tags/all", handler.QueryAllTags)
	router.GET("/tags", handler.ListTags)

	m.Handle("/", router)

	// run m
	if err := m.Run(); err != nil {
		log.Fatal(err)
	}
}
