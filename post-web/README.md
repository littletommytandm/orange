# Post Service

This is the Post service

Generated with

```
micro new github.com/littletommytan/orange/post-web --namespace=go.micro --fqdn=orange.web.post --type=web
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: orange.web.post
- Type: web
- Alias: post

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./post-web
```

Build a docker image
```
make docker
```