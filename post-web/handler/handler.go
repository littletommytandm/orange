package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/client"
	"net/http"
	postPb "orange/post-web/proto/post"
)

var (
	postClient postPb.PostService
)

func New() {
	postClient = postPb.NewPostService("orange.srv.post", client.DefaultClient)
}

// Error 错误结构体
type Error struct {
	Message string `json:"message"`
}

func CreatePost(c *gin.Context) {
	type Post struct {
		Title string   `json:"title" binding:"required"`
		Body  string   `json:"body" binding:"required"`
		Tags  []string `json:"tags" binding:"required"`
	}
	var payload Post
	if err := c.ShouldBind(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": &Error{Message: "payload validation failed"}})
		return
	}
	rsp, _ := postClient.CreatePost(context.TODO(), &postPb.CreatePostRequest{Title: payload.Title, Body: payload.Body, Tags: payload.Tags})
	if rsp.Success != true {
		c.JSON(500, gin.H{"success": false, "error": &Error{Message: "internal server error"}})
		return
	}
	c.JSON(http.StatusOK, gin.H{"success": rsp.Success, "post": rsp.Post})
}
func ListPosts(c *gin.Context) {
	rsp, _ := postClient.QueryAllPosts(context.TODO(), &postPb.Request{})
	if rsp.Success != true {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": &Error{Message: "internal server error"}})
		return
	}
	c.JSON(200, gin.H{"success": rsp.Success, "posts": rsp.Posts})
}
func QueryAllTags(c *gin.Context) {
	rsp, _ := postClient.QueryAllTags(context.TODO(), &postPb.Request{})
	if rsp.Success != true {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": &Error{Message: "internal server error"}})
		return
	}
	c.JSON(200, gin.H{"success": rsp.Success, "tags": rsp.Tags})
}
func ListTags(c *gin.Context) {
	rsp, _ := postClient.ListTags(context.TODO(), &postPb.Request{})
	if rsp.Success != true {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": &Error{Message: "internal server error"}})
		return
	}
	c.JSON(200, gin.H{"success": rsp.Success, "tags": rsp.Tags})
}
