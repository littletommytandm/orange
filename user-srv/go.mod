module orange/user-srv

go 1.12

require (
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/golang/protobuf v1.3.2
	github.com/jinzhu/gorm v1.9.10
	github.com/micro/cli v0.2.0
	github.com/micro/go-micro v1.8.3
)
