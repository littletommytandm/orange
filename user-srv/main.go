package main

import (
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"
	"orange/user-srv/basic"
	"orange/user-srv/handler"
	"orange/user-srv/service"

	userPb "orange/user-srv/proto/user"
)

func main() {
	// New Basic
	basic.New()

	// New Service
	m := micro.NewService(
		micro.Name("orange.srv.user"),
		micro.Version("latest"),
	)

	// Init service
	m.Init(
		micro.Action(func(context *cli.Context) {
			service.New()
			handler.New()
		}))

	// Register Handler
	_ = userPb.RegisterUserHandler(m.Server(), &handler.User{})

	// Run service
	if err := m.Run(); err != nil {
		log.Fatal(err)
	}
}
