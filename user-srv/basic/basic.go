package basic

import (
	"orange/user-srv/basic/config"
	db "orange/user-srv/basic/dao"
)

func New() {
	config.New()
	db.New()
}
