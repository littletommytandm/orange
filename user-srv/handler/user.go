package handler

import (
	"context"
	"github.com/micro/go-micro/util/log"
	"orange/user-srv/service/user"

	userPb "orange/user-srv/proto/user"
)

type User struct{}

var (
	userService user.Service
)

func New() {
	var err error
	userService, err = user.GetService()
	if err != nil {
		log.Fatal("[Handler.New]初始化handler.service错误")
		return
	}
}

func (h *User) CreateUser(ctx context.Context, req *userPb.CreateUserRequest, rsp *userPb.CreateUserResponse) error {
	u, err := userService.CreateUser(req.UserName, req.Email, req.Pwd)
	if err != nil {
		log.Logf("[Handler.User.CreateUser] 创建用户失败 %s", err)
		rsp.Success = false
		rsp.Error = err
		return nil
	}
	log.Logf(`[Handler.User.CreateUser] user %s created`, req.UserName)
	rsp.Success = true
	rsp.User = u
	return nil
}

func (h *User) QueryUserByName(ctx context.Context, req *userPb.Request, rsp *userPb.Response) error {
	u, err := userService.QueryUserByName(req.UserName)
	if err != nil {
		log.Logf("[Handler.User.QueryUserByName] 查询用户失败 %s", err)
		if err.Code == 10003 {
			// 用户不存在，置success true,user nil
			rsp.Success = true
			rsp.User = &userPb.User{}
			rsp.Error = err
			return nil
		}
		rsp.Success = false
		rsp.Error = err
		return nil
	}
	log.Logf("[Handler.User.QueryUserByName] user %s found", req.UserName)
	rsp.Success = true
	rsp.User = u
	return nil
}
