package user

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	UserName string `gorm:"not null;unique"`
	Email    string `gorm:"not null;unique"`
	Pwd      string
}
