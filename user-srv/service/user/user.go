package user

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"orange/user-srv/basic/dao"
	userPb "orange/user-srv/proto/user"
	"sync"
)

var (
	svc   *service
	mutex sync.RWMutex
)

type service struct{}

// Service 用户服务接口
type Service interface {
	QueryUserByName(userName string) (user *userPb.User, error *userPb.Error)                      // QueryUserByName 根据用户名获取用户
	CreateUser(userName string, email string, pwd string) (user *userPb.User, error *userPb.Error) // CreateUser 创建一个用户
}

// GetService 获取用户服务体
func GetService() (Service, error) {
	if svc == nil {
		return nil, fmt.Errorf("[GetService] GetService 未初始化")
	}
	return svc, nil
}

// New 初始化用户服务体
func New() {
	mutex.Lock()
	defer mutex.Unlock()

	if svc != nil {
		return
	}
	svc = &service{}
}

func (s *service) QueryUserByName(userName string) (user *userPb.User, error *userPb.Error) {
	user = &userPb.User{}

	user.UserName = userName
	if err := dao.GetDB().Where("user_name = ?", userName).First(&user).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &userPb.Error{Code: 10003, Message: err.Error()}
		} else {
			return nil, &userPb.Error{Code: 10004, Message: err.Error()}
		}
	}
	return user, nil
}

func (s *service) CreateUser(userName string, email string, pwd string) (user *userPb.User, error *userPb.Error) {
	user = &userPb.User{}
	u := &User{UserName: userName, Email: email, Pwd: pwd}

	if err := dao.GetDB().Where("user_name = ?", userName).Or("email = ?", email).First(&user).Error; err != nil {
		if !gorm.IsRecordNotFoundError(err) {
			return nil, &userPb.Error{Code: 999, Message: err.Error()}
		}
	} else {
		// 用户已存在
		return nil, &userPb.Error{Code: 10001, Message: `user existed`}
	}

	if err := dao.GetDB().Create(&u).Error; err != nil {
		return nil, &userPb.Error{Code: 10002, Message: err.Error()}
	}
	b, _ := json.Marshal(u)
	err := json.Unmarshal(b, user)
	if err != nil {
		return nil, &userPb.Error{Code: 1}
	}
	return user, nil
}
