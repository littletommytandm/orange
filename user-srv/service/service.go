package service

import (
	"orange/user-srv/basic/dao"
	"orange/user-srv/service/user"
)

// service 初始化接口
func New() {
	user.New()
	dao.GetDB().AutoMigrate(&user.User{})
}
