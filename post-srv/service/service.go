package service

import (
	"orange/post-srv/basic/dao"
	"orange/post-srv/service/post"
)

// service 初始化接口
func New() {
	post.New()
	dao.GetDB().AutoMigrate(&post.Post{})
	dao.GetDB().AutoMigrate(&post.Tag{})
}
