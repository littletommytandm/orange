package post

import (
	"time"
)

type Post struct {
	ID        int64 `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	Title     string     `gorm:"not null"`
	Body      string     `gorm:"not null"`
	Tags      []Tag      `gorm:"ForeignKey:PostID"`
}

type Tag struct {
	ID        int64 `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	Name      string     `gorm:"not null"`
	PostID    int64      `gorm:"not null"`
}

type TagCollection struct {
	Name  string
	Total int64
}
