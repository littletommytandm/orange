package post

import (
	"encoding/json"
	"fmt"
	"orange/post-srv/basic/dao"
	postPb "orange/post-srv/proto/post"
	"sync"
)

var (
	svc   *service
	mutex sync.RWMutex
)

type service struct{}

// Service 博文服务接口
type Service interface {
	QueryAllPosts() (posts []*postPb.Post, error *postPb.Error)
	QueryAllTags() (tags []*postPb.Tag, error *postPb.Error)
	ListTags() (tags []*postPb.TagCollection, error *postPb.Error)
	CreatePost(title string, body string, tags []string) (post *postPb.Post, error *postPb.Error)
}

// GetService 获取用户服务体
func GetService() (Service, error) {
	if svc == nil {
		return nil, fmt.Errorf("[GetService] GetService 未初始化")
	}
	return svc, nil
}

// New 初始化用户服务体
func New() {
	mutex.Lock()
	defer mutex.Unlock()

	if svc != nil {
		return
	}
	svc = &service{}
}

func (s service) QueryAllPosts() (posts []*postPb.Post, error *postPb.Error) {
	var tps []*Post
	posts = []*postPb.Post{}

	if err := dao.GetDB().Find(&tps).Error; err != nil {
		return nil, &postPb.Error{Code: 999, Message: err.Error()}
	}

	for _, p := range tps {
		if err := dao.GetDB().Model(&p).Association("Tags").Find(&p.Tags).Error; err != nil {
			return nil, &postPb.Error{Code: 999, Message: err.Error()}
		}

		t := &postPb.Post{}
		b, _ := json.Marshal(p)
		err := json.Unmarshal(b, t)
		if err != nil {
			return nil, &postPb.Error{Code: 1, Message: err.Error()}
		}
		posts = append(posts, t)
	}

	return posts, nil
}

func (s service) QueryAllTags() (tags []*postPb.Tag, error *postPb.Error) {
	var tempTags []*Tag
	if err := dao.GetDB().Find(&tempTags).Error; err != nil {
		return nil, &postPb.Error{Code: 999, Message: err.Error()}
	}
	for _, tag := range tempTags {
		t := &postPb.Tag{}

		b, _ := json.Marshal(tag)
		err := json.Unmarshal(b, t)

		if err != nil {
			return nil, &postPb.Error{Code: 1, Message: err.Error()}
		}

		tags = append(tags, t)
	}
	return tags, nil
}

func (s service) ListTags() (tags []*postPb.TagCollection, error *postPb.Error) {
	const sql = `select t."name",count(*) total from tags t group by t."name"`
	rows, err := dao.GetDB().Raw(sql).Rows()
	if err != nil {
		return nil, &postPb.Error{Code: 999, Message: err.Error()}
	}
	defer rows.Close()
	for rows.Next() {
		tc := &TagCollection{}
		ptc := &postPb.TagCollection{}
		_ = dao.GetDB().ScanRows(rows, tc)

		b, _ := json.Marshal(tc)
		err := json.Unmarshal(b, ptc)
		if err != nil {
			return nil, &postPb.Error{Code: 1, Message: err.Error()}
		}
		tags = append(tags, ptc)
	}
	return tags, nil
}

func (s service) CreatePost(title string, body string, tags []string) (post *postPb.Post, error *postPb.Error) {
	p := &Post{Title: title, Body: body}

	if err := dao.GetDB().Create(&p).Error; err != nil {
		return nil, &postPb.Error{Code: 999, Message: err.Error()}
	}
	var params []Tag
	for _, tag := range tags {
		params = append(params, Tag{Name: tag})
	}
	if err := dao.GetDB().Model(&p).Association("Tags").Append(params).Error; err != nil {
		return nil, &postPb.Error{Code: 999, Message: err.Error()}
	}

	post = &postPb.Post{}
	b, _ := json.Marshal(p)
	err := json.Unmarshal(b, post)
	if err != nil {
		return nil, &postPb.Error{Code: 1, Message: err.Error()}
	}
	return post, nil
}
