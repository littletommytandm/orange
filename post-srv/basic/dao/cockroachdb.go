package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/micro/go-micro/util/log"
	"orange/post-srv/basic/config"
)

func initCockroachDB() *gorm.DB {
	// 创建连接
	connection, err := gorm.Open("postgres", config.GetDBConfig().GetDSN())
	if err != nil {
		log.Fatal(err)
		panic(err)
	}

	// 测试连接
	if err := connection.DB().Ping(); err != nil {
		log.Fatal(err)
	}

	log.Logf(`[DB.New] 数据库加载成功`)
	return connection
}
