package dao

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro/util/log"
	"orange/post-srv/basic/config"
	"sync"
)

var (
	isUp     bool
	db       *gorm.DB
	redisCli *redis.Client
	m        sync.RWMutex
)

// New 初始化数据库
func New() {
	m.Lock()
	defer m.Unlock()

	if isUp {
		err := fmt.Errorf("[DB.New] database 已经初始化过")
		log.Logf(err.Error())
		return
	}

	// 如果配置声明使用mysql
	if config.GetDBConfig().GetEnabled() {
		db = initCockroachDB()
	}

	isUp = true
}

// GetDB 获取db
func GetDB() *gorm.DB {
	return db
}

// GetRedis 获取redis
func GetRedis() *redis.Client {
	return redisCli
}
