package config

// DBConfig db 配置 接口
type DBConfig interface {
	GetDSN() string
	GetEnabled() bool
}

// defaultDBConfig 数据库配置
type defaultDBConfig struct {
	DSN    string `json:"dsn"`
	Enable bool   `json:"enabled"`
}

// GetDSN 连接数据源
func (db defaultDBConfig) GetDSN() string {
	return db.DSN
}

// Enabled 激活
func (db defaultDBConfig) GetEnabled() bool {
	return db.Enable
}

//// 闲置连接数
//func (db defaultDBConfig) GetMaxIdleConnection() int {
//	return db.MaxIdleConnection
//}
//
//// 打开连接数
//func (db defaultDBConfig) GetMaxOpenConnection() int {
//	return db.MaxOpenConnection
//}
