package config

import (
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/micro/go-micro/config"
	"github.com/micro/go-micro/config/source"
	"github.com/micro/go-micro/config/source/file"
	"github.com/micro/go-micro/util/log"
)

var (
	defaultRootPath         = "app"
	defaultConfigFilePrefix = "application-"
	//consulConfig            defaultConsulConfig
	dbConfig defaultDBConfig
	profiles defaultProfiles
	mutex    sync.RWMutex
	isUp     bool
)

const sp = string(filepath.Separator)

// New 初始化配置
func New() {
	mutex.Lock()
	defer mutex.Unlock()

	if isUp {
		log.Logf("[New] 配置已经初始化过")
		return
	}

	// 加载yml配置
	// 先加载基础配置
	appPath, _ := filepath.Abs(filepath.Dir(filepath.Join("."+sp, sp)))

	pt := filepath.Join(appPath, "conf")
	_ = os.Chdir(appPath)

	// 找到application.yml文件
	if err := config.Load(file.NewSource(file.WithPath(pt + sp + "application.yml"))); err != nil {
		panic(err)
	}

	// 找到需要引入的新配置文件
	if err := config.Get(defaultRootPath, "profiles").Scan(&profiles); err != nil {
		panic(err)
	}

	log.Logf("[Config.New] 加载配置文件：path: %s, %+v\n", pt+sp+"application.yml", profiles)

	// 开始导入新文件
	if len(profiles.GetInclude()) > 0 {
		include := strings.Split(profiles.GetInclude(), ",")

		sources := make([]source.Source, len(include))
		for i := 0; i < len(include); i++ {
			filePath := pt + string(filepath.Separator) + defaultConfigFilePrefix + strings.TrimSpace(include[i]) + ".yml"

			log.Logf("[Config.New] 加载配置文件：path: %s\n", filePath)

			sources[i] = file.NewSource(file.WithPath(filePath))
		}

		// 加载include的文件
		if err := config.Load(sources...); err != nil {
			panic(err)
		}
	}

	// 赋值
	//_ = config.Get(defaultRootPath, go"consul").Scan(&consulConfig)
	_ = config.Get(defaultRootPath, "cockroachdb").Scan(&dbConfig)

	// 标记已经初始化
	isUp = true
}

// GetDBConfig 获取mysql配置
func GetDBConfig() DBConfig {
	return dbConfig
}

//// GetConsulConfig 获取Consul配置
//func GetConsulConfig() (ret ConsulConfig) {
//	return consulConfig
//}
