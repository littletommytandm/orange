package basic

import (
	"orange/post-srv/basic/config"
	db "orange/post-srv/basic/dao"
)

func New() {
	config.New()
	db.New()
}
