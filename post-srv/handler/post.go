package handler

import (
	"context"
	"fmt"
	"github.com/micro/go-micro/util/log"
	"orange/post-srv/service/post"

	postPb "orange/post-srv/proto/post"
)

type Post struct{}

var (
	postService post.Service
)

func New() {
	var err error
	postService, err = post.GetService()
	if err != nil {
		log.Fatal("[Handler.Post.New]初始化handler.service错误")
		return
	}
}

func (e *Post) QueryAllPosts(ctx context.Context, req *postPb.Request, rsp *postPb.QueryPostsResponse) error {
	posts, err := postService.QueryAllPosts()
	fmt.Println(posts)
	if err != nil {
		log.Logf("[Handler.Post.QueryAllPosts] list posts error %s", err)
		rsp.Success = false
		rsp.Error = err
		return nil
	}
	log.Logf(`[Handler.Post.QueryAllPosts] query all posts`)
	rsp.Success = true
	rsp.Posts = posts
	return nil
}

func (e *Post) QueryAllTags(ctx context.Context, req *postPb.Request, rsp *postPb.QueryTagsResponse) error {
	tags, err := postService.QueryAllTags()
	if err != nil {
		log.Logf("[Handler.Post.QueryAllTags] list tags error %s", err)
		rsp.Success = false
		rsp.Error = err
		return nil
	}
	log.Logf(`[Handler.Post.QueryAllTags] query all tags`)
	rsp.Success = true
	rsp.Tags = tags
	return nil
}

func (e *Post) ListTags(ctx context.Context, req *postPb.Request, rsp *postPb.ListTagsResponse) error {
	tags, err := postService.ListTags()
	if err != nil {
		log.Logf("[Handler.Post.ListTags] list tags error %s", err)
		rsp.Success = false
		rsp.Error = err
		return nil
	}
	log.Logf(`[Handler.Post.ListTags] list all tags`)
	rsp.Success = true
	rsp.Tags = tags
	return nil
}

func (e *Post) CreatePost(ctx context.Context, req *postPb.CreatePostRequest, rsp *postPb.CreatePostResponse) error {
	p, err := postService.CreatePost(req.Title, req.Body, req.Tags)
	if err != nil {
		log.Logf("[Handler.Post.CreatePost] post create error %s", err)
		rsp.Success = false
		rsp.Error = err
		return nil
	}
	log.Logf(`[Handler.Post.CreatePost] post %s created`, req.Title)
	rsp.Success = true
	rsp.Post = p
	return nil
}
