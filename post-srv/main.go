package main

import (
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/util/log"
	"orange/post-srv/basic"
	"orange/post-srv/handler"
	"orange/post-srv/service"

	postPb "orange/post-srv/proto/post"
)

func main() {
	// New Basic
	basic.New()

	// New Service
	m := micro.NewService(
		micro.Name("orange.srv.post"),
		micro.Version("latest"),
	)

	// init m
	m.Init(
		micro.Action(func(context *cli.Context) {
			service.New()
			handler.New()
		}))

	// Register Handler
	_ = postPb.RegisterPostHandler(m.Server(), &handler.Post{})

	// Run m
	if err := m.Run(); err != nil {
		log.Fatal(err)
	}
}
